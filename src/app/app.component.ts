import { Component } from '@angular/core';
import { SocketService } from './services/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  message = '';
  receives = [];

  constructor(
    private _socket: SocketService
  ) {
    this._socket.getMessage().subscribe(
      message => this.receives.push(message)
    );
  }

  onClick() {
    this._socket.sendMessage(this.message);
  }
}
