import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private _socket: Socket) { }

  sendMessage(msg: string) {
    this._socket.emit('notification', msg);
  }

  getMessage() {
    return this._socket
      .fromEvent<any>('notification').pipe(
        map((data) => data)
      );
  }
}
